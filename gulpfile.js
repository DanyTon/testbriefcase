'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass');
const browserSync = require('browser-sync');

gulp.task('sass', function() {
  gulp
    .src("./public/css/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./public/css/"));
});

gulp.task('sass:watch', function() {
  gulp.watch("./public/css/*.scss", ['sass']);
});

gulp.task('browser-symc', function() {
  var files = ['./*.html', './public/css/*.css', './public/images/*.{png, jpg, gift}', './js/*.js']
  browserSync.init(files{
    server: {
      baseDir: './'
    }
  });
});

gulp.task('default', ['browser-sync'], function(){
  gulp.setMaxListeners('sasa:watch');
})